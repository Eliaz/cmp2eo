<?php get_header(); ?>
<div class="main-container">
    <div class="main wrapper clearfix">

        <h2>Welkom op index.php</h2>

        <?php if(have_posts()): ?>

            <?php while(have_posts())
            {
                the_post();

                echo '<h1>';
                the_title();
                echo '</h1>';

                the_content();

            } ?>

        <?php else: ?>

            Er is geen inhoud gevonden.

        <?php endif; ?>

        <!-- Hier komt de sidebar -->
        <?php get_sidebar(); ?>

    </div> <!-- #main -->
</div> <!-- #main-container -->

<?php get_footer(); ?>
