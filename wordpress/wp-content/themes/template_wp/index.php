<?php get_header(); ?>
<div class="main-container">
    <div class="main wrapper clearfix">

        <div class="content">

            <?php if(have_posts()): ?>
                <?php $counter = 3;
                $recentPosts = new WP_Query();
                $recentPosts->query('showposts=3');
                ?>

                <?php while ($recentPosts->have_posts()) : $recentPosts->the_post(); ?>
                    <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <div class="box<?php echo $counter--; ?>">
                        <h4 class="date"><?php the_date(); ?></h4>
                        <?php the_content(); ?>
                    </div>
                <?php endwhile; ?>
        <?php else: ?>

            Er is geen inhoud gevonden.

        <?php endif; ?>
        </div>

        <!-- Hier komt de sidebar -->
        <?php get_sidebar(); ?>

    </div> <!-- #main -->
</div> <!-- #main-container -->

<?php get_footer(); ?>
