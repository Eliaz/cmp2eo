<!--
 * Created by PhpStorm.
 * User: EliazBruggeman
 * Date: 5/05/16
 * Time: 16:59
 -->
<?php get_header(); ?>
    <div class="main-container">
        <div class="main wrapper clearfix">
            <div class="content">
                <?php if(have_posts()): ?>

                    <?php while(have_posts()) {

                        the_post();
                        the_time('F jS, Y');echo'.'?>&nbsp;<?php the_author();
                        echo '<h1>';
                        the_title();
                        echo '</h1>';

                        the_content();

                        comments_template();
                    } ?>

                    <?php else: ?>

                    Er is geen inhoud gevonden.

                <?php endif; ?>
            </div>
            <!-- Hier komt de sidebar -->
            <?php get_sidebar(); ?>

        </div> <!-- #main -->
    </div> <!-- #main-container -->

<?php get_footer(); ?>